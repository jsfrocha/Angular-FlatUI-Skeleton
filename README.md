## AngularJS + FlatUI (w/ Bootstrap) Skeleton Application

#### Includes:

* jQuery (http://jquery.com)
* Bootstrap (http://getbootstrap.com)
* Flat-UI (http://designmodo.github.io/Flat-UI/)
* AngularJS (https://angularjs.org/)

#### File Structure:

````
AngBootstrapFlatSkeleton
├── index.html
├── app
|	├── views
|	|	└── main.html
|	|	└── ...
│   ├── controllers
│   │   └── mainCtrl.js
│   │   └── ...
│   ├── directives
│   │   └── myDirective.js
│   │   └── ...
│   ├── filters
│   │   └── myFilter.js
│   │   └── ...
│   ├── services
│   │   └── myService.js
│   │   └── ...
│   ├── vendor
│   │   ├── angular.js
│   │   ├── angular.min.js
│   │   ├── es5-shim.min.js
│   │   └── json3.min.js
│   └── app.js
├── css
│   └── bootstrap
|	└── flatui
|	└── fonts
|	└── styles
|		└── main.css
├── js
│   └── angular
|	└── bootstrap
|	└── flatui
|	└── jquery
|		└── main.css
└── ...
````
