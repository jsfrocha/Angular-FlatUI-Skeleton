'use strict';

var myApp = angular.module('myApp', [
	'ngRoute',
	'myApp.controllers'
]);

var controllers = angular.module('myApp.controllers', []);

myApp.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/', {
				templateUrl:'app/views/main.html',
				controller:'TestCtrl'
			});
	}
]);
